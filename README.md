# Ansible playbook practice
##  Requirements 
- Python
- Ansible
- SSH


## How to run
```
$ ansible-playbook playbookName.yaml
```
## Team
- Carlos Tomás García Martínez      |   320605
- Héctor René Aguirre Chávez        |   320793
- Ericka Pamela Bermúdez Pillado    |   282478
